Exchange rate data processing
-
* Program downloads data from NBP API.

**INPUT**
* currency code `(USD|EUR|CHF|GBP)` 
* time period 

**OUTPUT**
* outputs **average** bid rate
* **standard deviation** of ask rate
