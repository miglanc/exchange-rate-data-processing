public class Rate {

    private Object no, effectiveDate;
    private double bid, ask;

    public double getBid() {
        return bid;
    }

    public double getAsk() {
        return ask;
    }
    /*
    {
    "no":"209/C/NBP/2018",
    "effectiveDate":"2018-10-26",
    "bid":4.2686,
    "ask":4.3548
    }
     */
}
