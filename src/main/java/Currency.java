import java.util.List;

public class Currency {

    private Object table, currency, code;
    private List<Rate> rates;

    public List<Rate> getRates() {
        return rates;
    }
    /*
    {
    "table":"C",
    "currency":"euro",
    "code":"EUR",
    "rates":[{"no":"209/C/NBP/2018","effectiveDate":"2018-10-26","bid":4.2686,"ask":4.3548}]
    }
     */
}
