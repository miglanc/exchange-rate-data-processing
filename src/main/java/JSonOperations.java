import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class JSonOperations {

    private static Gson gson = new Gson();

    double getRate(String json, String rate) {
        double rateFromJson=0;
        Currency currency = gson.fromJson(json, Currency.class);
        switch (rate) {
            case "bid":
                rateFromJson = currency.getRates().get(0).getBid();
                break;
            case "ask":
                rateFromJson = currency.getRates().get(0).getAsk();
                break;
        }
        return rateFromJson;
    }

    boolean checkAvailabilityOfData(String currency, String date) throws IOException {
        boolean check = true;
        try {
            getStringJSonFromUrl(currency, date);
        } catch (FileNotFoundException e) {
            e.getStackTrace();
            check = false;
        }
        return check;
    }

    String getStringJSonFromUrl(String currency, String date) throws IOException {
        String urlNbp = "http://api.nbp.pl/api/exchangerates/rates/c/" + currency + "/" + date;
        URL url = new URL(urlNbp);
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("Accept", "application/json");
        InputStream is = connection.getInputStream();
        Scanner sc = new Scanner(is);
        return sc.nextLine();
    }
}
