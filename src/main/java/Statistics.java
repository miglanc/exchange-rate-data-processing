import java.util.List;

public class Statistics {

    static double standardDeviation(List<Double> list) {
        double sigma = 0;
        double average = avg(list);
        for (double n: list) {
            sigma += Math.pow(n-average,2);
        }
        return roundToDecimalPlace(Math.sqrt(sigma/list.size()),4);
    }

    static double avg(List<Double> list) {
        double avg=0;
        for (double d: list) {
            avg += d;
        }
        return roundToDecimalPlace(avg/list.size(),4);
    }

    private static double roundToDecimalPlace(double number, int decimalPlace) {
        double powerOfTen = Math.pow(10, decimalPlace);
        return Math.round(number*powerOfTen)/powerOfTen;
    }
}
