import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        boolean check, isDateCorrect = false;
        String start, end, currency;
        String dateRegex = "\\d{4}-\\d{2}-\\d{2}";

        List<Double> bidList = new ArrayList<>();
        List<Double> askList = new ArrayList<>();
        List<String> currencies = new ArrayList<>(Arrays.asList("USD", "EUR", "CHF", "GBP"));
        JSonOperations jso = new JSonOperations();
        Scanner sc = new Scanner(System.in);

        while (!isDateCorrect) {
            System.out.println("Select currency [USD|EUR|CHF|GBP]: ");
            currency = sc.nextLine().toUpperCase();
            System.out.println("Enter start date [yyyy-mm-dd]: ");
            start = sc.nextLine();
            System.out.println("Enter end date [yyyy-mm-dd]: ");
            end = sc.nextLine();
            if (!start.matches(dateRegex) || !end.matches(dateRegex) || !currencies.contains(currency)) {
                System.out.println("Incorrect date or currency format!");
            } else {
                LocalDate startDate = LocalDate.parse(start);
                LocalDate endDate = LocalDate.parse(end).plusDays(1);
                if (startDate.isBefore(endDate)) {
                    while (!startDate.equals(endDate)) {
                        check = jso.checkAvailabilityOfData(currency, start);
                        if (check) {
                            String json = jso.getStringJSonFromUrl(currency, start);
                            bidList.add(jso.getRate(json, "bid"));
                            askList.add(jso.getRate(json, "ask"));
                        }
                        startDate = startDate.plusDays(1);
                        start = startDate.toString();
                    }
                    if (bidList.isEmpty()) {
                        System.out.println("No data in this period!");
                    } else {
                        System.out.println("Average bid rate: " + Statistics.avg(bidList));
                        System.out.println("Standard deviation of ask rate: " + Statistics.standardDeviation(askList));
                        isDateCorrect = true;
                    }
                } else {
                    System.out.println("Start date should be before end date!");
                }
            }
        }
    }
}
